describe('Unit: MainController', function() {
	beforeEach(module('mylo.controllers'));

	var ctrl, scope;
	beforeEach(inject(function($controller, $rootScope) {
		scope = $rootScope.$new();
		ctrl = $controller('MainController', {
			$scope: scope
		});
	}));
	
});