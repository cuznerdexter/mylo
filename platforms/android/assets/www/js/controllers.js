angular.module('mylo.controllers', [])

    .controller('MainController', ['$scope', '$state', '$rootScope', '$ionicPlatform', '$ionicPopup', '$timeout', 'ServicesAuth', 'ServicesConnect', 'ServicesData', 'notifyService', '$cordovaLocalNotification', '$ionicSideMenuDelegate', '$firebase',
        function ($scope, $state, $rootScope, $ionicPlatform, $ionicPopup, $timeout, ServicesAuth, ServicesConnect, ServicesData, notifyService, $cordovaLocalNotification, $ionicSideMenuDelegate, $firebase) {


            $scope.parentObj = {};
            $scope.parentObj.authRef = ServicesAuth.authRef;
            $scope.parentObj.auth = ServicesAuth.Auth;
            $scope.parentObj.user = $scope.parentObj.auth.$getAuth();
            $scope.parentObj.loginState = $scope.parentObj.auth.$getAuth();
            $scope.parentObj.connectState = ServicesConnect.getConnect();
            $scope.parentObj.simplelogin = '';
            $scope.parentObj.online = true;

            $scope.parentObj.alarmTime = '';

            $scope.parentObj.displayName = ServicesAuth.CurrName;
            $scope.parentObj.yepNopePage = '';
            $scope.parentObj.homeState = '';
            $scope.parentObj.colorVal1 = 'redBg';
            $scope.parentObj.colorVal2 = 'greenBg';
            $scope.parentObj.colorVal3 = 'blueBg';

            $scope.parentObj.reflection = '';
            $scope.parentObj.advice = '';

            $scope.notifyMessages = notifyService.getMessages();

            $scope.parentObj.init = function () {
                $scope.parentObj.dragbool = $state.current.views.mainContent.data.dragbool;
                $scope.parentObj.yepNopePage = $state.current.views.mainContent.data.yepNopePage;
                $scope.parentObj.colorVal1 = $state.current.views.mainContent.data.colorVal[0];
                $scope.parentObj.colorVal2 = $state.current.views.mainContent.data.colorVal[1];
                $scope.parentObj.colorVal3 = $state.current.views.mainContent.data.colorVal[2];
                $scope.parentObj.homeState = $state.current.views.mainContent.data.homeState;
            };

            //$ionicPlatform.ready(function() {
            if(navigator.splashscreen) {
                navigator.splashscreen.hide();
            }

            $scope.$on("$cordovaLocalNotification:schedule", function (event, notification, state) {
            });
            $scope.$on("NOTIFYTIME", function (alarmData) {
                $scope.parentObj.alarmTime = alarmData.targetScope.notifyTime;

                var str = $scope.parentObj.alarmTime.toString();
                str = str.replace(/[/]/g, ',');
                var d = str.toLocaleString('en-US');
                $scope.parentObj.alarmTime = d;
                $scope.addNote($scope.parentObj.alarmTime);
            });

            $scope.addNote = function (alarm) {
                $cordovaLocalNotification.schedule({
                    id: 1,
                    title: 'How was your meditation today?',
                    text:   notifyService.message,
                    every: 'day',
                    led: 'FD0174'
                }).then(function () {
                    console.log("The notification has been set");
                });
            };

            $scope.isScheduled = function () {
                $cordovaLocalNotification.isScheduled(1).then(function (isScheduled) {
                });
            };

            document.addEventListener("menubutton", onMenuKeyDown, false);
            document.addEventListener("backbutton", onBackKeyDown, false);
            document.addEventListener("pause", pauseApp, false);
            document.addEventListener("resume", resumeApp, false);



            //});

            function onMenuKeyDown () {
                //activated when menu button pressed
                console.log('menu button pressed - toggle menu');
                $ionicSideMenuDelegate.toggleLeft();
            }

            function onBackKeyDown () {
                //activated when back button pressed
                if(($state.$current.name == 'app.home') || ($state.$current.name == 'app.login'))
                    window.navigator.app.exitApp()
                console.log('back button pressed');
            }

            function pauseApp () {
                console.log('MYLO has been paused');
                Firebase.goOffline();
            }

            function resumeApp () {
                console.log('MYLO has resumed');
                Firebase.goOnline();
            }
        }])

    .controller('LoginCtrl', ['$scope', '$state', '$q', '$timeout', 'ServicesAuth', 'ServicesConnect', 'ServicesData',
        function ($scope, $state, $q, $timeout, ServicesAuth, ServicesConnect, ServicesData) {

            $scope.parentObj.init();

            $scope.parentObj.connectState = ServicesConnect.getConnect();

            $scope.form = {};
            $scope.form.email = '';
            $scope.form.password = '';

            $scope.loginGrp = ServicesAuth.loginGrp;

            $scope.goHome = function(error, authData) {
                if(authData)
                {
                } else {
                }
            };

            $scope.createUser = function () {
                $scope.message = null;

                $scope.parentObj.auth.$createUser({
                    email: $scope.form.email,
                    password: $scope.form.password
                }).then(function(userData) {
                    $scope.message = "User created with uid: " + userData.uid;
                    $scope.loginWithPass();
                    ServicesData.autoresponder($scope.form.email,ServicesData.sendy.list.autoresponder);
                }).catch(function(error) {
                    $scope.error = error;
                });
            };

            $scope.removeUser = function () {
                $scope.message = null;
                $scope.error = null;

                $scope.parentObj.auth.$removeUser({
                    email: $scope.form.email,
                    password: $scope.form.password
                }).then(function() {
                    $scope.message = "User removed";
                }).catch(function(error) {
                    $scope.error = error;
                });
            };

            $scope.loginWithPass = function () {
                $scope.message = null;
                $scope.error = null;
                // console.log(JSON.stringify($scope.form));
                $scope.parentObj.authRef.authWithPassword({
                    email: $scope.form.email,
                    password: $scope.form.password
                }, function(error, authData) {
                    // alert("werer")
                    if (error) {
                        console.log("Login Failed!", error);
                    } else {
                        console.log("Authenticated successfully with payload:", authData);
                        ServicesData.setupData(true);
                    }
                });
            };

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };

            $scope.Anonamous = function() {
                ServicesAuth.Anonamous();
                ServicesAuth.UpdateData();
                console.log("login Anonamous");
                $state.go('app.home');
            };

            $scope.parentObj.loginState = $scope.parentObj.auth.$getAuth();
            $scope.parentObj.user = $scope.parentObj.auth.$getAuth();
        }])

    .controller('IntroCtrl', ['$scope','$state', '$ionicSlideBoxDelegate', '$localstorage',
        function ($scope, $state, $ionicSlideBoxDelegate, $localstorage) {

            var isFirstTime = $localstorage.getObject("firstLoaded");
            if(isFirstTime == "yes") {
                $state.go('app.home');
                return;
            }
            $localstorage.setObject( "firstLoaded", "yes" );

            $scope.parentObj.firstRun = !$scope.parentObj.firstRun;

            $scope.startApp = function() {
                $state.go('app.home');
            };
            $scope.next = function() {
                $ionicSlideBoxDelegate.next();
            };
            $scope.previous = function() {
                $ionicSlideBoxDelegate.previous();
            };
            $scope.slideChanged = function(index) {
                $scope.slideIndex = index;
            };
        }])

    .controller('AppCtrl', ['$scope', '$cordovaDatePicker', '$ionicPlatform', 'ServicesAuth', 'notifyService',
        function ($scope, $cordovaDatePicker, $ionicPlatform, ServicesAuth, notifyService ) {

            $scope.notifyState = false;
            $scope.reminderState = false;
            $scope.mobileMode = window.cordova ? true : false;
            $scope.notify = "OFF";
            $scope.notifyTime = "Set the time";
            $scope.parentObj.loginState = $scope.parentObj.auth.$getAuth();
            $scope.logState = ['Login',false];

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };

            $scope.$watch( function (scope) {return $scope.parentObj.loginState;},
                function (newValue, oldValue) {
                    var result = document.getElementsByClassName("myloPrivate");
                    var wrappedQueryResult = angular.element(result);
                    if(newValue !== null) {
                        $scope.logState = ['Logout',true];
                        wrappedQueryResult.removeClass('myloSideDisabled');
                    }else {
                        $scope.logState = ['Login',false];
                        wrappedQueryResult.addClass('myloSideDisabled');
                    }
                }
            );

            $scope.setReminderPanel = function () {
                $scope.reminderState = !$scope.reminderState;
            };

            $scope.setNotify = function () {
                $scope.notifyState = !$scope.notifyState;
                if($scope.notifyState)
                {
                    $scope.notify = "ON";
                }else
                {
                    $scope.notify = "OFF";
                }
            };

            $scope.externalLink = function (url) {
                if($ionicPlatform)
                {
                    window.open(url, "_system");
                }else {
                    console.log('needs ngCordova inAppBrowser');
                }
            };

            ionic.Platform.ready(function(){
                // will execute when device is ready, or immediately if the device is already ready.
                alert("dsf");
            });

            $(window).ready(function () {
                alert('sdfdg');
            });
            $ionicPlatform.ready(function() {
                alert("aya");
                $scope.setNotificationTime = function () {
                    alert('aya3');
                    var options = {
                        date: new Date(),
                        mode: 'time', // or 'time'
                        minDate: new Date() - 10000,
                        allowOldDates: true,
                        allowFutureDates: true,
                        doneButtonLabel: 'DONE',
                        doneButtonColor: '#F2F3F4',
                        cancelButtonLabel: 'CANCEL',
                        cancelButtonColor: '#000000'
                    };
                    //document.addEventListener("deviceready", function () {

                    $cordovaDatePicker.show(options).then(function(date){
                        //alert(date);
                    });

                    //}, false);
                    //$cordovaDatePicker.show(options).then(function(date){
                    //    alert(date);
                    //    $scope.notifyTime = options.date;
                    //    $scope.$emit('NOTIFYTIME', $scope.notifyTime);
                    //});
                };
                alert("aya2");
            });
        }])

    .controller('HomeCtrl', ['$scope', '$state', 'ServicesData', 'ServicesAuth',
        function ($scope, $state, ServicesData, ServicesAuth) {
            $scope.parentObj.init();
            $scope.linkNewEntry = $state.current.views.mainContent.data.myloLinks.newEntry;
            $scope.linkViewStats = $state.current.views.mainContent.data.myloLinks.viewStats;

            $scope.parentObj.loginState = $scope.parentObj.auth.$getAuth();
            $scope.parentObj.user = $scope.parentObj.auth.$getAuth();

            if(!$scope.parentObj.loginState) {
                $state.go('app.login');
            }

            $scope.createEntry = function () {
                ServicesData.setStatsInit();
            };

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };

            $scope.gotoHow = function() {
                $state.go('app.meditationHow');
            }

            $scope.gotoGuide = function() {
                $state.go('app.meditationGuide');
            }
        }])


    .controller('MeditatedCtrl', ['$scope', '$state', 'reflectionService', 'ServicesData', '$ionicHistory', 'ServicesAuth',
        function ($scope, $state, reflectionService, ServicesData, $ionicHistory, ServicesAuth) {
            $scope.parentObj.init();
            $scope.medEventYes = $state.current.views.mainContent.data.myloLinks.medEventYes;
            $scope.medEventNo = $state.current.views.mainContent.data.myloLinks.medEventNo;

            $scope.setAnswer = function (answer) {
                $scope.answered(answer);
            };
            $scope.onSwipeAcross = function (answer) {
                $scope.answered(answer);
            };
            $scope.answered = function (answer) {
                if(answer == 'yes'){
                    ServicesData.setStatsMed(true);
                    $state.go('app.meditationLength');
                }else {
                    ServicesData.setStatsMed(false);
                    $state.go('app.home');
                }
            };
            $scope.myGoBack = function() {
                // $ionicHistory.goBack();
                window.history.back();
            };
            reflectionService.dataType = 0;
            $scope.logOut = function() {
                ServicesAuth.Logout();
            };
        }])

    .controller('MeditationLengthCtrl', ['$scope', '$state', 'reflectionService', 'medLengthService', 'ServicesData', '$ionicHistory', 'ServicesAuth',
        function ($scope, $state, reflectionService, medLengthService, ServicesData, $ionicHistory, ServicesAuth) {
            $scope.parentObj.init();
            $scope.currYposition = 0;
            if(medLengthService.timeSliderNum == 'undefined')
            {medLengthService.timeSliderNum = 0;}
            $scope.ansNum = medLengthService.timeSliderNum;
            $scope.timeSlots = [
                {
                    length: 5,
                    label: '5'
                },
                {
                    length: 10,
                    label: '10'
                },
                {
                    length: 15,
                    label: '15'
                },
                {
                    length: 20,
                    label: '20'
                },
                {
                    length: 25,
                    label: '25'
                },
                {
                    length: 30,
                    label: '30'
                },
                {
                    length: 35,
                    label: '35'
                },
                {
                    length: 40,
                    label: '40'
                },
                {
                    length: 45,
                    label: '45+'
                }
            ];

            $scope.updateypos = function () {
                $scope.currYposition = medLengthService.timeSliderVal;
            };
            $scope.setAnswer = function () {
                $scope.answered();
            };
            $scope.onSwipeAcross = function () {
                $scope.answered();
            };
            $scope.answered = function (length) {
                $scope.updateypos();
                $scope.ansNum = medLengthService.timeSliderNum;
                $scope.currYposition = length;
                $scope.ansNum = length;
                if($scope.ansNum < 5 ){
                    ServicesData.setStatsMedLen($scope.ansNum);
                    $state.go('app.meditationReflect');
                }else {
                    ServicesData.setStatsMedLen($scope.ansNum);
                    $state.go('app.meditationThoughtless');
                }
            };
            $scope.myGoBack = function() {
                // $ionicHistory.goBack();
                window.history.back();
            };
            reflectionService.dataType = 1;

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };
        }])



    .controller('MeditationThoughtlessCtrl', ['$scope', '$state', 'reflectionService', 'ServicesData', '$ionicHistory', 'ServicesAuth',
        function ($scope, $state, reflectionService, ServicesData, $ionicHistory, ServicesAuth) {
            $scope.parentObj.init();
            $scope.medEventYes = $state.current.views.mainContent.data.myloLinks.medEventYes;
            $scope.medEventNo = $state.current.views.mainContent.data.myloLinks.medEventNo;

            $scope.setAnswer = function (answer) {
                $scope.answered(answer);
            };
            $scope.onSwipeAcross = function (answer) {
                $scope.answered(answer);
            };
            $scope.answered = function (answer) {
                if(answer == 'yes'){
                    ServicesData.setStatsMedTho(true);
                    // $state.go('app.meditationDay');
                }else {
                    ServicesData.setStatsMedTho(false);
                    // $state.go('app.meditationReflect');
                }
                $state.go('app.meditationDay');
            };
            $scope.myGoBack = function() {
                // $ionicHistory.goBack();
                window.history.back();
            };
            reflectionService.dataType = 2;

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };
        }])


    .controller('MeditationDayCtrl', ['$scope', '$state', 'reflectionService', 'dayQualityService', 'ServicesData', '$ionicHistory', 'ServicesAuth',
        function ($scope, $state, reflectionService, dayQualityService, ServicesData, $ionicHistory, ServicesAuth) {
            $scope.parentObj.init();

            $scope.updateypos = function () {
                $scope.currYposition = dayQualityService.timeSliderVal;
            };

            $scope.setAnswer = function (choice) {
                // alert(choice);
                dayQualityService.timeSliderNum = choice;
                $scope.currYposition = choice;
                $scope.answered();
            };

            $scope.answered = function () {
                switch (dayQualityService.timeSliderNum) {
                    case 100:
                        dayQualityService.dayValue = 'good';
                        ServicesData.setStatsMedQual(35);
                        break;
                    case 50:
                        dayQualityService.dayValue = 'average';
                        ServicesData.setStatsMedQual(17);
                        break;
                    case 0:
                        dayQualityService.dayValue = 'poor';
                        ServicesData.setStatsMedQual(5);
                        break;
                }
                $state.go('app.meditationStats');
            };
            $scope.myGoBack = function() {
                // $ionicHistory.goBack();
                window.history.back();
            };

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };
        }])



    .controller('MeditationStatsCtrl', ['$scope', '$state', 'ServicesData', 'loadData', '$timeout', '$ionicHistory', 'ServicesAuth', '$ionicPlatform',
        function ($scope, $state, ServicesData, loadData, $timeout, $ionicHistory, ServicesAuth, $ionicPlatform ) {
            $scope.parentObj.init();
            $scope.medEventAdvice = $state.current.views.mainContent.data.myloLinks.medEventAdvice;

            $scope.drawGraph = function() {
                $scope.stats = ServicesData.getJSON();
                console.log("Drawing graph");

                if(($scope.stats == undefined) || ($scope.stats == undefined)) return;

                var keys = Object.keys($scope.stats);
                for (var i = 0; i < keys.length - 1; i ++) {
                    for (var j = i + 1; j < keys.length; j ++) {
                        var moment1 = moment(keys[i], "YYYY-MM-DD-HH-mm");
                        var moment2 = moment(keys[j], "YYYY-MM-DD-HH-mm");
                        if ( moment1 < moment2 ) {
                            var temp = keys[i];
                            keys[i] = keys[j];
                            keys[j] = temp;
                        }
                    }
                }

                $scope.jsonData = Array();

                for( var i = 0; i < keys.length; i ++ )
                {
                    var date = keys[i];
                    var statData = $scope.stats[date];
                    var date_moment = moment(date, "YYYY-MM-DD-HH-mm");
                    if(!date_moment.isValid()) continue;
                    // console.log(JSON.stringify(date_moment));
                    $scope.monthTitle = date_moment.format("MMM YYYY");
                    $scope.monthName = date_moment.format("MMM");
                    if((date !== null) && (date !== undefined) && (date !== "null") && (date !== "undefined") && (date !== "")  )
                    {
                        var arr2str = String($scope.stats[date].medDate);
                        date_moment = moment(arr2str, "YYY-MM-DD-HH-mm");
                        // if(date_moment.isValid()) {
                        var cutStr = date_moment.format("DD MMM");
                        statData.medDate = cutStr; //remove end chars
                        // }
                    }

                    if((statData.medLen == null) || (statData.medLen == undefined) || (statData.medLen == 0)) {
                        statData.medLen = 0;
                        continue;
                    } else {

                    }

                    if((statData.medTho !== null) && (statData.medTho !== undefined)) {
                        if(statData.medTho == 1) {
                            statData.medThoStr = "Yes";
                        } else {
                            statData.medThoStr = "No";
                        }
                    } else {
                        statData.medTho = 0;
                        statData.medThoStr = "no_width";
                    }

                    if((statData.medUql !== null) && (statData.medUql !== undefined)) {
                        if(statData.medUql == 5) {
                            statData.medUqlStr = "Poor";
                            statData.medUqlClass = "poor_rating";
                        } else if(statData.medUql == 17) {
                            statData.medUqlStr = "Average";
                            statData.medUqlClass = "average_rating";
                        } else if(statData.medUql == 35) {
                            statData.medUqlStr = "Good";
                            statData.medUqlClass = "good_rating";
                        }
                    } else {
                        statData.medUql = 0;
                        statData.medUqlClass = "no_width";
                    }
                    $scope.jsonData.push(statData);
                }
            };

            //$ionicPlatform.ready(function() {
            $scope.stats =  ServicesData.getJSON();
            $scope.myloKey = ServicesData.mylokey;
            $scope.drawGraph();
            console.log($scope.jsonData);
            //});

            $scope.onSwipeAcross = function (answer) {
                if(answer == 'yes'){
                    $state.go('app.meditationAdvice');
                }else {
                    $state.go('app.home');
                }
            };

            $scope.myGoBack = function() {
                // console.log($ionicHistory);
                // $ionicHistory.goBack();
                window.history.back();
            };

            $scope.goHome = function() {
                $state.go('app.home');
            };

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };
        }])


    .controller('MeditationReflectCtrl', ['$scope', '$state', 'reflectionService', 'ServicesAuth',
        function ($scope, $state, reflectionService, ServicesAuth) {
            $scope.parentObj.init();

            $scope.setReflection = function () {
                $scope.parentObj.reflection = reflectionService.dataSet[reflectionService.dataType][0];
            };

            $scope.viewStats = function () {
                $state.go('app.meditationStats');
            };
            $scope.setReflection();

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };
        }])

    .controller('MeditationAdviceCtrl', ['$scope', '$state', 'adviceService', 'ServicesAuth',
        function ($scope, $state, adviceService, ServicesAuth) {
            $scope.parentObj.init();
            $scope.medEventHome = $state.current.views.mainContent.data.myloLinks.medEventHome;

            $scope.adviceNum = 0;
            function mesgeMix (min, max) {
                var rand = Math.floor(Math.random() * (max - min)) + min;
                $scope.adviceNum = rand;
                return rand;
            }

            $scope.setAdvice = function () {
                $scope.parentObj.advice = adviceService.dataSet[mesgeMix(0, adviceService.dataSet.length )][0];
            };
            $scope.setAdvice();

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };
        }])

    .controller('MeditationHowCtrl', ['$scope', '$state', 'ServicesAuth', '$ionicHistory',
        function ($scope, $state, ServicesAuth, $ionicHistory) {
            $scope.parentObj.init();

            $scope.nxtPage = function () {
                $state.go('app.meditationGuide');
            };

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };

            $scope.myGoBack = function() {
                // $ionicHistory.goBack();
                window.history.back();
            }
        }])

    .controller('MeditationGuideCtrl', ['$scope', '$rootScope', '$state', 'ServicesAuth', '$ionicHistory', '$ionicPlatform', '$cordovaMedia',
        function ($scope, $rootScope, $state, ServicesAuth, $ionicHistory, $ionicPlatform, $cordovaMedia) {
            $scope.parentObj.init();
            if($rootScope.isPlaying == "undefined")
                $rootScope.isPlaying = false;
            var src = 'https://audiohub.s3.amazonaws.com/MeditationSantoor.mp3';

            //$ionicPlatform.ready(function() {
            if(!$rootScope.media) {
                $rootScope.media = new Media(src, null, null, mediaStatusCallback);
            }
            //});

            $scope.nxtPage = function () {
                $state.go('app.home');
            };

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };

            $scope.myGoBack = function() {
                // $ionicHistory.goBack();
                window.history.back();
            }

            $scope.togglePlay = function() {
                if($rootScope.isPlaying) {
                    $rootScope.media.pause();
                }
                else {
                    $rootScope.media.play();
                }
                $rootScope.isPlaying = !$rootScope.isPlaying;
            }

            var mediaStatusCallback = function(status) {
                if(status == 1) {
                    $ionicLoading.show({template: 'Loading...'});
                } else {
                    $ionicLoading.hide();
                }
            }
        }])

    .controller('MeditationFeedbackCtrl', ['$scope', '$state', '$cordovaEmailComposer', '$ionicPlatform', '$location', '$anchorScroll', 'ServicesAuth',
        function ($scope, $state, $cordovaEmailComposer, $ionicPlatform, $location, $anchorScroll, ServicesAuth) {
            $scope.parentObj.init();

            $scope.feedback = {};

            $scope.pageShift = function (event, dir) {
                $('ion-content').animate({
                    scrollTop: dir
                }, 750);
            };


            $scope.submitComments = function () {
                if(window.cordova) {

                    //$ionicPlatform.ready(function() {
                    $cordovaEmailComposer.isAvailable().then(function() {
                        console.log($scope.feedback);
                        $cordovaEmailComposer.open({
                            to: 'cuznerdexter@gmail.com',
                            cc: [$scope.feedback.email, 'nigelp@gmail.com'],
                            bcc: [],
                            attachments: [],
                            subject: 'Mylo Feedback',
                            body: $scope.feedback.user +'<br>'+ $scope.feedback.name +'<br>'+ $scope.feedback.email +'<br>'+ $scope.feedback.comments,
                            isHtml: true
                        });
                    }, function () {
                        // not available
                        console.log('email isNOTAvailable ');
                    });

                    //}, function(){
                    //    console.log('ionic is not ready, is this a mobile device?');
                    //});

                } else {
                    console.log('no ionic');
                }
            };

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };
        }])


    .controller('AboutCtrl', [ '$scope', 'aboutData', '$timeout', 'ServicesAuth',
        function ($scope, aboutData, $timeout, ServicesAuth) {
            console.log('AboutCtrl::');


            $scope.aboutData = aboutData.data.aboutPage.content;
            console.log($scope.aboutData);

            $scope.logOut = function() {
                ServicesAuth.Logout();
            };

        }]);
